/*
  Copyright (C) 2019 Federico Braghiroli
  Author: Federico Braghiroli <federico.braghiroli@gmail.com>

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <nuttx/config.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>

#include <unistd.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <debug.h>
#include <fcntl.h>
#include <ctype.h>

#include <nuttx/wireless/nrf24l01.h>
#include <nuttx/analog/adc.h>
#include <nuttx/analog/ioctl.h>
#include <nuttx/ioexpander/gpio.h>
#include <nuttx/timers/rtc.h>

#include <sys/boardctl.h>

#include "adcbr_protocol/adcbr_protocol.h"

#define RF_DEV_NAME   "/dev/nrf24l01"
#define ADC_DEV_NAME  "/dev/adc0"
#define GPOUT0_DEV_NAME "/dev/gpout0"
#define GPOUT1_DEV_NAME "/dev/gpout1"
#define ALARM_DEV_NAME "/dev/rtc0"

#define DEFAULT_RADIOFREQ  2450
#define DEFAULT_TXPOWER    -6    /* (0, -6, -12, or -18 dBm) */

#define ADC_N_CH	10

#define SLEEP_DURATION		20
#define WAKEUP_ALARM_SIGNO	1

#define SENSORS_PWRON_DELAY	200 /* ms */

#define eprintf(...) fprintf(stderr, __VA_ARGS__)
#define dprintf(...) fprintf(stderr, __VA_ARGS__)

struct opt {
	uint8_t debug_sel;
	uint8_t help_sel;
};

static int wireless_cfg(int fd);
static int send_pkt(int wl_fd, uint8_t *b, size_t s);
static void usage(void);

static const uint8_t defaultaddr[NRF24L01_MAX_ADDR_LEN] = {
	0x01, 0xCA, 0xFE, 0x12, 0x34
};

static uint8_t buff[NRF24L01_MAX_PAYLOAD_LEN + 1] = "";
struct adc_msg_s sample[ADC_N_CH];
static struct opt g_opt;

static int parse_args(int argc, char *argv[], struct opt *o)
{
	int index;
	int c;

	memset(o, 0, sizeof(struct opt));

	while ((c = getopt (argc, argv, "dh")) != -1) {
		switch (c) {
		case 'd':
			o->debug_sel = 1;
			break;
		case 'h':
			o->help_sel = 1;
			break;
		case '?':
			if (isprint (optopt))
				eprintf("Unknown option `-%c'.\n",
					optopt);
			else
				eprintf("Unknown option character"
					" `\\x%x'.\n", optopt);
			return 1;
		default:
			return 1;
		}
	}

	if (o->help_sel)
		return 0;

	for (index = optind; index < argc; index++) {
		fprintf(stderr, "Invalid parameter: %s\n", argv[index]);
		return 1;
	}

	return 0;
}

static int wireless_cfg(int fd)
{
	int error = 0;

	uint32_t rf = DEFAULT_RADIOFREQ;
	int32_t txpow = DEFAULT_TXPOWER;
	nrf24l01_datarate_t datarate = RATE_1Mbps;

	nrf24l01_retrcfg_t retrcfg = {
		.count = 5,
		.delay = DELAY_1000us
	};

	uint32_t addrwidth = NRF24L01_MAX_ADDR_LEN;

	uint8_t pipes_en = (1 << 0);  /* pipe 0 only */
	nrf24l01_pipecfg_t pipe0cfg;
	nrf24l01_pipecfg_t *pipes_cfg[NRF24L01_PIPE_COUNT] =
		{&pipe0cfg, 0, 0, 0, 0, 0};

	nrf24l01_state_t primrxstate;

	primrxstate = ST_STANDBY;

	pipe0cfg.en_aa = true;
	pipe0cfg.payload_length = NRF24L01_DYN_LENGTH;
	memcpy (pipe0cfg.rx_addr, defaultaddr, NRF24L01_MAX_ADDR_LEN);

	ioctl(fd, WLIOC_SETRADIOFREQ, (unsigned long)((uint32_t *)&rf));
	ioctl(fd, WLIOC_SETTXPOWER, (unsigned long)((int32_t *)&txpow));
	ioctl(fd, NRF24L01IOC_SETDATARATE,
	      (unsigned long)((nrf24l01_datarate_t *)&datarate));
	ioctl(fd, NRF24L01IOC_SETRETRCFG,
	      (unsigned long)((nrf24l01_retrcfg_t *)&retrcfg));
	ioctl(fd, NRF24L01IOC_SETADDRWIDTH,
	      (unsigned long)((uint32_t*)&addrwidth));
	ioctl(fd, NRF24L01IOC_SETTXADDR,
	      (unsigned long)((uint8_t *)&defaultaddr));
	ioctl(fd, NRF24L01IOC_SETPIPESCFG,
	      (unsigned long)((nrf24l01_pipecfg_t **)&pipes_cfg));

	ioctl(fd, NRF24L01IOC_SETPIPESENABLED,
	      (unsigned long)((uint8_t *)&pipes_en));
	ioctl(fd, NRF24L01IOC_SETSTATE,
	      (unsigned long)((nrf24l01_state_t *)&primrxstate));

	return error;
}

static int send_pkt(int wl_fd, uint8_t *b, size_t s)
{
	int ret;

	ret = write(wl_fd, b, s);
	if (ret < 0) {
		eprintf("Error sending packet\n");
		return ret;
	} else {
		int retrcount;
		ioctl(wl_fd, NRF24L01IOC_GETLASTXMITCOUNT,
			     (unsigned long)((uint32_t *)&retrcount));
		dprintf("Packet sent successfully !  "
			"(%d retransmitted packets)\n", retrcount);
	}

	return OK;
}

static void usage(void)
{
	printf("Orto node program.\n\n");
}

static inline int samp_n_send(int adc_fd, int rf_fd, struct opt *o)
{
	int nsamples, i;
	size_t retsize;
	uint8_t *pkt_p;

	if (ioctl(adc_fd, ANIOC_TRIGGER, 0) < 0) {
		eprintf("ANIOC_TRIGGER ioctl failed: %d\n", errno);
		return -errno;
	}
	usleep(1000);
	retsize = read(adc_fd, sample, ADC_N_CH *
		       sizeof(struct adc_msg_s));
	if (retsize < 0) {
		eprintf("read() error: %d\n", -errno);
		return -errno;
	} else if (retsize == 0) {
		dprintf("no data from read()\n");
		return -errno;
	}
	nsamples = retsize / sizeof(struct adc_msg_s);
	pkt_p = buff;
	pkt_p += adcbr_build_pkt_head(buff, NRF24L01_MAX_PAYLOAD_LEN,
				      nsamples*sizeof(uint16_t), APT_HUMIDITY);
	for (i = 0; i < nsamples; i++) {
		pkt_p = adcbr_build_payload(pkt_p, sample[i].am_channel,
					    sample[i].am_data);
		if (o->debug_sel)
			dprintf("ch:%02u d:%04u\n", sample[i].am_channel,
				sample[i].am_data);
	}
	send_pkt(rf_fd, buff, pkt_p - buff);

	return 0;
}

#if 1
static inline void stay_on(uint8_t v)
{
	struct boardioc_pm_ctrl_s ctrl = {};
	ctrl.state = PM_NORMAL;
	ctrl.domain = 0;
	if (v)
		ctrl.action = BOARDIOC_PM_STAY;
	else
		ctrl.action = BOARDIOC_PM_RELAX;
	boardctl(BOARDIOC_PM_CONTROL, (uintptr_t)&ctrl);

	return;
}
#endif

static inline int set_alm(int fd, time_t s)
{
	struct rtc_setrelative_s sr;
	int ret = 0;


	sr.id = 0;
	sr.pid = 0;
	sr.reltime = s;
	sr.event.sigev_notify = SIGEV_SIGNAL;
	sr.event.sigev_signo = WAKEUP_ALARM_SIGNO;
	sr.event.sigev_value.sival_int = 0;
	ret = ioctl(fd, RTC_SET_RELATIVE, (unsigned long)((uintptr_t)&sr));

	if (ret < 0)
		ret = -errno;

	return ret;
}

#ifdef BUILD_MODULE
int main(int argc, FAR char *argv[])
#else
int orto_node_main(int argc, char *argv[])
#endif
{
	int rf_fd, adc_fd, gpout0_fd, gpout1_fd, rtc_fd;
	int ret = 0;
	sigset_t sset;

	sigemptyset(&sset);
	sigaddset(&sset, WAKEUP_ALARM_SIGNO);

	if (parse_args(argc, argv, &g_opt))
		goto exit_help;
	if (g_opt.help_sel)
		goto exit_help;

	if ((rtc_fd = open(ALARM_DEV_NAME, O_WRONLY)) < 0) {
		ret = -errno;
		eprintf("%s open failed: %d\n", ALARM_DEV_NAME, ret);
		return ret;
	}

	rf_fd = open(RF_DEV_NAME, O_RDWR);
	if (rf_fd < 0) {
		ret = -errno;
		eprintf("%s open failed: %d\n", RF_DEV_NAME, ret);
		goto exit_close;
	}

	adc_fd = open(ADC_DEV_NAME, O_RDONLY);
	if (adc_fd < 0) {
		ret = -errno;
		eprintf("%s open failed: %d\n", ADC_DEV_NAME, ret);
		goto exit_close;
	}

	gpout0_fd = open(GPOUT0_DEV_NAME, O_RDWR);
	if (gpout0_fd < 0) {
		ret = -errno;
		eprintf("%s open failed: %d\n", GPOUT0_DEV_NAME, ret);
		goto exit_close;
	}

	gpout1_fd = open(GPOUT1_DEV_NAME, O_RDWR);
	if (gpout1_fd < 0) {
		ret = -errno;
		eprintf("%s open failed: %d\n", GPOUT1_DEV_NAME, ret);
	        goto exit_close;
	}


	if (argc != 1)
		usage();

	wireless_cfg(rf_fd);
	ioctl(gpout0_fd, GPIOC_WRITE, 0);
	ioctl(gpout1_fd, GPIOC_WRITE, 0);

	while (1) {
		static int cnt = 0;
		int alm_ret, sig_ret;
		stay_on(1);
		ioctl(gpout0_fd, GPIOC_WRITE, 1);
		ioctl(gpout1_fd, GPIOC_WRITE, 1);
		/* Enough time to get stable reads from sensors */
		usleep(SENSORS_PWRON_DELAY*1000);
		samp_n_send(adc_fd, rf_fd, &g_opt);
		ioctl(gpout0_fd, GPIOC_WRITE, 0);
		ioctl(gpout1_fd, GPIOC_WRITE, 0);
		stay_on(0);
		if ((alm_ret = set_alm(rtc_fd, SLEEP_DURATION)) < 0) {
			eprintf("set alarm fail: %d\n", alm_ret);
			eprintf("sleep aborted\n");
			continue;
		}
		sig_ret = sigtimedwait(&sset, NULL, NULL);
		if (sig_ret != WAKEUP_ALARM_SIGNO) {
			eprintf("sigtimedwait unexpected: %d\n", sig_ret);
			sleep(SLEEP_DURATION);
		}
		printf("\nw:%d\n", cnt++);
	}
exit_help:
	usage();
	return 0;

exit_close:
	close(adc_fd);
	close(rf_fd);
	close(gpout0_fd);
	close(gpout1_fd);
	close(rtc_fd);
	dprintf ("Bye !\n");
	return ret;
}
